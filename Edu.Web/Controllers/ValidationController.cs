﻿using System;
using System.Threading.Tasks;
using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Controllers
{
    public class ValidationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public ValidationController(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [HttpGet]
        public async Task<IActionResult> VerifyParentUserName([Bind(Prefix = "Parent.UserName")] string userName, [Bind(Prefix = "Parent.Id")] Guid? id)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                if (id.HasValue)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    var parent = await _context.Parents.FindAsync(id);
                    return Json(parent.UserId == new Guid(userId));
                }

                return Json(false);
            }

            return Json(true);
        }

        [HttpGet]
        public async Task<IActionResult> VerifyTeacherUserName([Bind(Prefix = "Teacher.UserName")] string userName, [Bind(Prefix = "Teacher.Id")] Guid? id)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                if (id.HasValue)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    var parent = await _context.Teachers.FindAsync(id);
                    return Json(parent.UserId == new Guid(userId));
                }

                return Json(false);
            }

            return Json(true);
        }

        [HttpGet]
        public async Task<IActionResult> VerifyClassName([Bind(Prefix = "Class.Name")] string name, [Bind(Prefix = "Class.Id")] Guid? id)
        {
            var @class = await _context.Classes.SingleOrDefaultAsync(c => c.Name == name && c.Id != id);
            return Json(@class == null);
        }

        [HttpGet]
        public async Task<IActionResult> VerifyCourseName([Bind(Prefix = "Course.Name")] string name, [Bind(Prefix = "Course.Id")] Guid? id)
        {
            var @class = await _context.Courses.SingleOrDefaultAsync(c => c.Name == name && c.Id != id);
            return Json(@class == null);
        }

        [HttpGet]
        public async Task<IActionResult> VerifyTopicName([Bind(Prefix = "Topic.Name")] string name, [Bind(Prefix = "Topic.Id")] Guid? id)
        {
            var @class = await _context.Topics.SingleOrDefaultAsync(c => c.Name == name && c.Id != id);
            return Json(@class == null);
        }

        [HttpGet]
        public async Task<IActionResult> VerifyStudentNumber([Bind(Prefix = "Student.StudentNumber")] string studentNumber, [Bind(Prefix = "Student.Id")] Guid? id)
        {
            var @class = await _context.Students.SingleOrDefaultAsync(c => c.StudentNumber == studentNumber && c.Id != id);
            return Json(@class == null);
        }
    }
}
