﻿$(function () {
    if ($('#editor').length) {
        var editor = new Quill('#editor',
            {
                modules: {
                    toolbar: [
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                        ['bold', 'italic', 'underline', 'strike'],
                        [{ 'align': [] }],
                        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                        [{ 'script': 'sub' }, { 'script': 'super' }],
                        [{ 'color': [] }, { 'background': [] }],
                        ['link', 'image']
                    ]
                },
                theme: 'snow'
            });
        editor.on('text-change',
            function() {
                $('form[name="message"]').find('input:hidden#message').val(JSON.stringify(editor.getContents()));
            });
    }

    if ($('#viewer').length) {
        var viewer = new Quill('#viewer',
            {
                readOnly: true
            });
        viewer.setContents(JSON.parse($('input:hidden#message').val()));
    }
});