﻿namespace Edu.Web.Config
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string Teacher = "Teacher";
        public const string Parent = "Parent";
    }
}
