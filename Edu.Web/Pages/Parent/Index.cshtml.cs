﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Parent
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public IndexModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public readonly IDictionary<string, string> Messages = new Dictionary<string, string>();

        public async Task OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            var students = _context.Students.Include(s => s.Class).Where(s => s.Parent.UserId == new Guid(userId) && !string.IsNullOrWhiteSpace(s.Class.Message));
            foreach (var student in students)
            {
                Messages[student.Class.Name] = student.Class.Message;
            }
        }
    }
}