﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Parent.Student
{
    public class DetailsModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public DetailsModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public StudentViewModel Student { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student =
                await _context.Students.
                Include(s => s.Class.Courses).
                ThenInclude(c => c.Course.Topics).
                ThenInclude(t => t.Students).
                Include(s => s.Parent.User).
                Include(s => s.Lessons).
                ThenInclude(sl => sl.Lesson).
                ThenInclude(l => l.Documents).
                FirstOrDefaultAsync(s => s.Id == id);

            if (student == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            if (student.Parent.UserId != new Guid(userId))
                return Forbid();

            Student = new StudentViewModel
            {
                Name = student.Name,
                StudentNumber = student.StudentNumber,
                Class = student.Class.Name,
                Parent = student.Parent.User.Name,
                Courses = student.Class.Courses.Select(c => new StudentViewModel.CourseViewModel
                {
                    Id = c.CourseId,
                    Name = c.Course.Name,
                    Materials = c.Course.Materials,
                    Topics = c.Course.Topics.Select(t => new StudentViewModel.CourseViewModel.TopicViewModel
                    {
                        Name = t.Name,
                        Score = t.Students.SingleOrDefault(s => s.StudentId == id)?.Score,
                        Finished = t.Students.SingleOrDefault(s => s.StudentId == id)?.Finished,
                        Date = t.Students.SingleOrDefault(s => s.StudentId == id)?.Date
                    }).ToList()
                }).ToList(),
                Lessons = student.Lessons.OrderByDescending(l => l.Lesson.Date).Select(sl => new StudentViewModel.LessonViewModel
                {
                    Date = sl.Lesson.Date,
                    Documents = sl.Lesson.Documents.Select(d => new StudentViewModel.LessonViewModel.DocumentViewModel
                    {
                       Id = d.Id,
                       FileName = d.FileName
                    }).ToList(),
                    Present = sl.Present.GetValueOrDefault(false),
                    Late = sl.Late.GetValueOrDefault(false),
                    Behavior = sl.Behavior.GetValueOrDefault(StudentLesson.EBehavior.Normal),
                    Note = sl.Note
                }).ToList()
            };

            return Page();
        }
    }
}
