﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Message
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public IndexModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IList<MessageViewModel> Messages { get; set; }

        public async Task OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            Messages = await _context.Messages
                .AsNoTracking()
                .Include(t => t.Receiver)
                .Where(m => m.ReceiverId == new Guid(userId))
                .OrderByDescending(m => m.Date)
                .Select(m => new MessageViewModel
                {
                    Id = m.Id,
                    Receiver = m.Receiver.Name,
                    Title = m.Title,
                    Date = m.Date,
                    Read = m.Read
                })
                .ToListAsync();
        }
    }
}
