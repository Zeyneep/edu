﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Message
{
    [Authorize]
    public class ViewModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public ViewModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public MessageViewModel Message { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entity = await _context.Messages.FindAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);
            if (entity.ReceiverId != new Guid(userId))
            {
                return Forbid();
            }

            entity.Read = true;
            await _context.SaveChangesAsync();

            Message = new MessageViewModel
            {
                Title = entity.Title,
                Date = entity.Date,
                Content = entity.Content
            };

            return Page();
        }
    }
}
