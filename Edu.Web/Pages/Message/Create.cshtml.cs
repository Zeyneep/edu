﻿using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Message
{
    public class CreateModel : PageModel
    {
        private readonly Data.ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;
        private readonly IStringLocalizer _localizer;

        public CreateModel(Data.ApplicationDbContext context, UserManager<EduUser> userManager, IStringLocalizer localizer)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));
        }

        [Required]
        [BindProperty]
        public string Title { get; set; }
        [Required]
        [BindProperty]
        public string Content { get; set; }
        [Required]
        [BindProperty]
        public Guid[] Receivers { get; set; }

        public async Task OnGet()
        {
            await SetPersonsViewDataAsync();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await SetPersonsViewDataAsync();

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            foreach (var receiver in Receivers)
            {
                await _context.Messages.AddAsync(new Data.Domain.Message
                {
                    Title = Title,
                    Content = Content,
                    Date = DateTime.Now,
                    SenderId = new Guid(userId),
                    ReceiverId = receiver
                });
            }
            await _context.SaveChangesAsync();

            return Page();
        }

        private async Task SetPersonsViewDataAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);

            var persons = new List<ReceiverViewModel>();
            if (roles.Contains(Config.Roles.Administrator))
            {
                persons.Add(new ReceiverViewModel
                {
                    Name = $"{_localizer["Parents"]}:"
                });
                persons.AddRange(_context.Parents.Include(p => p.User).Select(p => new ReceiverViewModel
                {
                    UserId = p.UserId,
                    Name = p.User.Name
                }));
                persons.Add(new ReceiverViewModel
                {
                    Name = $"{_localizer["Teachers"]}:"
                });
                persons.AddRange(_context.Teachers.Include(t => t.User).Select(t => new ReceiverViewModel
                {
                    UserId = t.UserId,
                    Name = t.User.Name
                }));
            }
            else if (roles.Contains(Config.Roles.Teacher))
            {
                var userId = await _userManager.GetUserIdAsync(user);
                persons.Add(new ReceiverViewModel
                {
                    Name = $"{_localizer["Parents"]}:"
                });
                persons.AddRange(_context.Classes.Where(c => c.Teacher.UserId == new Guid(userId)).SelectMany(c => c.Students).Select(s => s.Parent).Include(p => p.User).Select(p => new ReceiverViewModel
                {
                    UserId = p.UserId,
                    Name = p.User.Name
                }));
            }

            ViewData["Persons"] = persons;
        }
    }
}