﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;

namespace Edu.Web.Pages
{
    public class LanguageModel : PageModel
    {
        public IActionResult OnGet(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1), IsEssential = true }
            );

            if(!string.IsNullOrWhiteSpace(returnUrl))
                return LocalRedirect(returnUrl);    

            return RedirectToPage("/Index");
        }
    }
}