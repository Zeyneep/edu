﻿using System;
using System.Threading.Tasks;
using Edu.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Edu.Web.Pages
{
    [Authorize]
    public class DownloadModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public DownloadModel(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IActionResult> OnGet(Guid documentId)
        {
            var document = await _context.Documents.FindAsync(documentId);
            if (document == null)
                return NotFound();

            return File(document.Data, document.ContentType, document.FileName);
        }
    }
}