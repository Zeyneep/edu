﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Teacher.Student
{
    public class DetailsModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public DetailsModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public StudentViewModel Student { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student =
                await _context.Students.
                Include(s => s.Class.Teacher).
                Include(s => s.Class.Courses).
                ThenInclude(c => c.Course.Topics).
                ThenInclude(t => t.Students).
                Include(s => s.Parent.User).
                FirstOrDefaultAsync(s => s.Id == id);

            if (student == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);
            if (student.Class.Teacher.UserId != new Guid(userId))
                return Forbid();

            Student = new StudentViewModel
            {
                Id = student.Id,
                Name = student.Name,
                StudentNumber = student.StudentNumber,
                Class = student.Class.Name,
                Parent = student.Parent.User.Name,
                Courses = student.Class.Courses.Select(c => new StudentViewModel.CourseViewModel
                {
                    Id = c.CourseId,
                    Name = c.Course.Name,
                    Materials = c.Course.Materials,
                    Topics = c.Course.Topics.Select(t => new StudentViewModel.CourseViewModel.TopicViewModel
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Score = t.Students.SingleOrDefault(s => s.StudentId == id)?.Score,
                        Finished = t.Students.SingleOrDefault(s => s.StudentId == id)?.Finished,
                        Date = t.Students.SingleOrDefault(s => s.StudentId == id)?.Date
                    }).ToList()
                }).ToList()
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                // delete old data
                _context.StudentTopics.RemoveRange(_context.StudentTopics.Where(st => st.StudentId == Student.Id));

                // enter new data
                if (Student.Courses != null)
                {
                    foreach (var course in Student.Courses)
                    {
                        if (course.Topics == null)
                            continue;

                        foreach (var topic in course.Topics)
                        {
                            _context.StudentTopics.Add(new StudentTopic
                            {
                                StudentId = Student.Id,
                                TopicId = topic.Id,
                                Score = topic.Score,
                                Finished = topic.Finished,
                                Date = topic.Date
                            });
                        }
                    }
                }

                await _context.SaveChangesAsync();
            }

            return Page();
        }
    }
}
