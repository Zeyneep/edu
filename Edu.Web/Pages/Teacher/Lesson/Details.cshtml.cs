﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Teacher.Lesson
{
    public class DetailsModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public DetailsModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public LessonViewModel Lesson { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lessons.Include(l => l.Class.Teacher).Include(l => l.Students).ThenInclude(sl => sl.Student).FirstOrDefaultAsync(l => l.Id == id);
            if (lesson == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);
            if (lesson.Class.Teacher.UserId != new Guid(userId))
                return Forbid();

            Lesson = new LessonViewModel
            {
                Id = lesson.Id,
                Date = lesson.Date,
                Students = new List<LessonViewModel.StudentViewModel>()
            };

            if (lesson.Students.Any())
            {
                foreach (var student in lesson.Students)
                {
                    Lesson.Students.Add(new LessonViewModel.StudentViewModel
                    {
                        Id = student.StudentId,
                        Name = student.Student.Name,
                        Present = student.Present.GetValueOrDefault(false),
                        Late = student.Late.GetValueOrDefault(false),
                        Behavior = student.Behavior,
                        Note = student.Note
                    });
                }
            }
            else
            {
                var students = _context.Students.Where(s => s.Class.Teacher.UserId == new Guid(userId));
                foreach (var student in students)
                {
                    Lesson.Students.Add(new LessonViewModel.StudentViewModel
                    {
                        Id = student.Id,
                        Name = student.Name
                    });
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                // delete old data
                _context.StudentLessons.RemoveRange(_context.StudentLessons.Where(sl => sl.LessonId == Lesson.Id));

                // enter new data
                foreach (var student in Lesson.Students)
                {
                    _context.StudentLessons.Add(new StudentLesson
                    {
                        LessonId = Lesson.Id,
                        StudentId = student.Id,
                        Behavior = student.Behavior,
                        Late = student.Late,
                        Present = student.Present,
                        Note = student.Note
                    });
                }

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Details", new { id = Lesson.Id });
        }
    }
}