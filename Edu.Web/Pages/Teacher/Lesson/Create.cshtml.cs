﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Pages.Teacher.Lesson
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public CreateModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);
            var teacher = await _context.Teachers.FirstOrDefaultAsync(t => t.UserId == new Guid(userId));

            if (!teacher.ClassId.HasValue)
                return RedirectToPage("./NoClass");

            Lesson = new Data.Domain.Lesson { ClassId = teacher.ClassId.Value, Date = DateTime.Today };
            return Page();
        }

        [BindProperty]
        public Data.Domain.Lesson Lesson { get; set; }
        [BindProperty]
        [Display(Name = "Document")]
        public IFormFile Document { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (Document != null && Document.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    await Document.CopyToAsync(ms);

                    Lesson.Documents = new List<Document>
                    {
                        new Document
                        {
                            Data = ms.ToArray(),
                            ContentType = Document.ContentType,
                            FileName = Document.FileName
                        }
                    };
                }
            }

            _context.Lessons.Add(Lesson);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new { Lesson.Id });
        }
    }
}