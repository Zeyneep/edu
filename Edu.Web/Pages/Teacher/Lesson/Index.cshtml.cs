﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Teacher.Lesson
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public IndexModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IList<Data.Domain.Lesson> Lessons { get; set; }

        public async Task OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            Lessons = await (from l in _context.Lessons.Include(l => l.Documents)
                             where l.Class.Teacher.UserId == new Guid(userId)
                             orderby l.Date descending
                             select l).ToListAsync();
        }
    }
}
