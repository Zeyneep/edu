﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Teacher
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public IndexModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public Guid? ClassId { get; set; }

        [BindProperty]
        [Display(Name = "Message")]
        public string Message { get; set; }

        public async Task OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = await _userManager.GetUserIdAsync(user);

            var @class = await _context.Classes.SingleAsync(c => c.Teacher.UserId == new Guid(userId));
            ClassId = @class.Id;
            Message = @class.Message;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var @class = await _context.Classes.FindAsync(ClassId);
            @class.Message = Message;
            await _context.SaveChangesAsync();

            return Page();
        }
    }
}