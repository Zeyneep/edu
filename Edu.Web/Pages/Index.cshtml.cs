﻿using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Edu.Web.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly UserManager<EduUser> _userManager;

        public IndexModel(UserManager<EduUser> userManager)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);

            if (roles.Contains(Config.Roles.Administrator))
                return RedirectToPage("/Admin/Index");

            if (roles.Contains(Config.Roles.Teacher))
                return RedirectToPage("/Teacher/Index");

            if (roles.Contains(Config.Roles.Parent))
                return RedirectToPage("/Parent/Index");

            return Forbid();
        }
    }
}
