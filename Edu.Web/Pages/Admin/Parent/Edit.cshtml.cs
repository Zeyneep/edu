﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;

namespace Edu.Web.Pages.Admin.Parent
{
    public class EditModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public EditModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public ParentViewModel Parent { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parent = await _context.Parents.Include(t => t.User).FirstOrDefaultAsync(m => m.Id == id);

            Parent = new ParentViewModel
            {
                Id = parent.Id,
                Email = parent.User.Email,
                UserName = parent.User.UserName,
                PhoneNumber = parent.User.PhoneNumber,
                Name = parent.User.Name,
                Address = parent.Address
            };

            if (Parent == null)
            {
                return NotFound();
            }
           ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var parent = await _context.Parents.FindAsync(Parent.Id);
            parent.Address = Parent.Address;

            var user = await _userManager.FindByIdAsync(parent.UserId.ToString());
            user.Name = Parent.Name;
            await _userManager.UpdateAsync(user);
            await _userManager.SetUserNameAsync(user, Parent.UserName);

            if (!string.IsNullOrWhiteSpace(Parent.Email))
            {
                await _userManager.SetEmailAsync(user, Parent.Email);
            }

            if (!string.IsNullOrWhiteSpace(Parent.PhoneNumber))
            {
                await _userManager.SetPhoneNumberAsync(user, Parent.PhoneNumber);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParentExists(Parent.Id))
                {
                    return NotFound();
                }

                throw;
            }

            return RedirectToPage("./Index");
        }

        private bool ParentExists(Guid id)
        {
            return _context.Parents.Any(e => e.Id == id);
        }
    }
}
