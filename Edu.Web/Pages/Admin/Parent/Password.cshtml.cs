﻿using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Admin.Parent
{
    public class PasswordModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public PasswordModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public PasswordViewModel Password { get; set; }

        public async Task<IActionResult> OnGet(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var parent = await _context.Parents.FindAsync(id);
            if (parent == null)
            {
                return NotFound();
            }

            Password = new PasswordViewModel
            {
                UserId = parent.UserId
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.FindByIdAsync(Password.UserId.ToString());
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, token, Password.Password);

            return RedirectToPage("./Index");
        }
    }
}