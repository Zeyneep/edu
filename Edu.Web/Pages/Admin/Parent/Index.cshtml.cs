﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Admin.Parent
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Data.Domain.Parent> Parent { get;set; }

        public async Task OnGetAsync()
        {
            Parent = await _context.Parents
                .Include(p => p.User).ToListAsync();
        }
    }
}
