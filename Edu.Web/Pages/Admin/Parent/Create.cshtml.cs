﻿using System;
using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;

namespace Edu.Web.Pages.Admin.Parent
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public CreateModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IActionResult OnGet()
        {
            ViewData["ClassId"] = new SelectList(_context.Users, "Id", "Name");
            return Page();
        }


        [BindProperty]
        public ParentViewModel Parent { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = new EduUser(Parent.UserName)
            {
                Name = Parent.Name
            };
            await _userManager.CreateAsync(user);
            await _userManager.AddToRoleAsync(user, Config.Roles.Parent);

            if (!string.IsNullOrWhiteSpace(Parent.Email))
            {
                await _userManager.SetEmailAsync(user, Parent.Email);
            }

            if (!string.IsNullOrWhiteSpace(Parent.PhoneNumber))
            {
                await _userManager.SetPhoneNumberAsync(user, Parent.PhoneNumber);
            }

            var parent = new Data.Domain.Parent
            {
                UserId = user.Id,
                Address = Parent.Address
            };

            _context.Parents.Add(parent);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}