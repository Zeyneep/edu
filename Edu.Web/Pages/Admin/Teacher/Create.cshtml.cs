﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Pages.Admin.Teacher
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public CreateModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IActionResult OnGet()
        {
            ViewData["ClassId"] = new SelectList(_context.Classes, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public TeacherViewModel Teacher { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = new EduUser(Teacher.UserName)
            {
                Name = Teacher.Name
            };
            await _userManager.CreateAsync(user);
            await _userManager.AddToRoleAsync(user, Config.Roles.Teacher);

            if (!string.IsNullOrWhiteSpace(Teacher.Email))
            {
                await _userManager.SetEmailAsync(user, Teacher.Email);
            }

            if (!string.IsNullOrWhiteSpace(Teacher.PhoneNumber))
            {
                await _userManager.SetPhoneNumberAsync(user, Teacher.PhoneNumber);
            }

            var teacher = new Data.Domain.Teacher
            {
                UserId = user.Id,
                Address = Teacher.Address
            };
            _context.Teachers.Add(teacher);

            await _context.SaveChangesAsync();

            if (Teacher.ClassId.HasValue)
            {
                var @class = await _context.Classes.FindAsync(Teacher.ClassId);

                await _context.Teachers.Where(t => t.ClassId == Teacher.ClassId).ForEachAsync(t => t.ClassId = null);

                teacher.ClassId = Teacher.ClassId;
                @class.TeacherId = teacher.Id;

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}