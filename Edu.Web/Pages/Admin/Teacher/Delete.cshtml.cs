﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;

namespace Edu.Web.Pages.Admin.Teacher
{
    public class DeleteModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public DeleteModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public Data.Domain.Teacher Teacher { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Teacher = await _context.Teachers
                .Include(t => t.Class)
                .Include(t => t.User).FirstOrDefaultAsync(m => m.Id == id);

            if (Teacher == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Teacher = await _context.Teachers.FindAsync(id);
            if (Teacher != null)
            {
                var user = await _userManager.FindByIdAsync(Teacher.UserId.ToString());
                var @class = await _context.Classes.SingleOrDefaultAsync(c => c.TeacherId == id);

                if (@class != null)
                    @class.TeacherId = null;
                _context.Teachers.Remove(Teacher);

                await _context.SaveChangesAsync();
                await _userManager.DeleteAsync(user);
            }

            return RedirectToPage("./Index");
        }
    }
}
