﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Identity;

namespace Edu.Web.Pages.Admin.Teacher
{
    public class EditModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public EditModel(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [BindProperty]
        public TeacherViewModel Teacher { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers
                .Include(t => t.Class)
                .Include(t => t.User).FirstOrDefaultAsync(m => m.Id == id);

            Teacher = new TeacherViewModel
            {
                Id = teacher.Id,
                UserName = teacher.User.UserName,
                Email = teacher.User.Email,
                ClassId = teacher.ClassId,
                Name = teacher.User.Name,
                PhoneNumber = teacher.User.PhoneNumber
            };

            if (Teacher == null)
            {
                return NotFound();
            }
            ViewData["ClassId"] = new SelectList(_context.Classes, "Id", "Name");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var teacher = await _context.Teachers.FindAsync(Teacher.Id);
            var @class = await _context.Classes.FindAsync(Teacher.ClassId);

            await _context.Teachers.Where(t => t.ClassId == Teacher.ClassId).ForEachAsync(t => t.ClassId = null);
            await _context.Classes.Where(c => c.TeacherId == teacher.Id).ForEachAsync(c => c.TeacherId = null);

            teacher.ClassId = Teacher.ClassId;
            teacher.Address = Teacher.Address;
            @class.TeacherId = teacher.Id;

            var user = await _userManager.FindByIdAsync(teacher.UserId.ToString());
            await _userManager.UpdateAsync(user);
            await _userManager.SetUserNameAsync(user, Teacher.UserName);
            user.Name = Teacher.Name;

            if (!string.IsNullOrWhiteSpace(Teacher.Email))
            {
                await _userManager.SetEmailAsync(user, Teacher.Email);
            }

            if (!string.IsNullOrWhiteSpace(Teacher.PhoneNumber))
            {
                await _userManager.SetPhoneNumberAsync(user, Teacher.PhoneNumber);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeacherExists(Teacher.Id))
                {
                    return NotFound();
                }

                throw;
            }

            return RedirectToPage("./Index");
        }

        private bool TeacherExists(Guid id)
        {
            return _context.Teachers.Any(e => e.Id == id);
        }
    }
}
