﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Edu.Web.Data;

namespace Edu.Web.Pages.Admin.Class
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Data.Domain.Class> Class { get;set; }

        public async Task OnGetAsync()
        {
            Class = await _context.Classes.Include(c => c.Teacher.User).ToListAsync();
        }
    }
}
