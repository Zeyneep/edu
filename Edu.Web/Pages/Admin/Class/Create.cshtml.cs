﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Admin.Class
{
    public class CreateModel : PageModel
    {
        private readonly Data.ApplicationDbContext _context;

        public CreateModel(Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["TeacherId"] = new SelectList(_context.Teachers.Include(t => t.User), "Id", "User.Name");
            return Page();
        }

        [BindProperty]
        public Data.Domain.Class Class { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var @class = new Data.Domain.Class
            {
                Name = Class.Name,
                Message = Class.Message
            };

            _context.Classes.Add(@class);
            await _context.SaveChangesAsync();

            if (Class.TeacherId.HasValue)
            {
                var teacher = await _context.Teachers.FindAsync(Class.TeacherId);

                await _context.Classes.Where(c => c.TeacherId == Class.TeacherId).ForEachAsync(t => t.TeacherId = null);

                @class.TeacherId = Class.TeacherId;
                teacher.ClassId = @class.Id;

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}