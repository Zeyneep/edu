﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Edu.Web.Pages.Admin.Class
{
    public class EditModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public EditModel(ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Data.Domain.Class Class { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Class = await _context.Classes.FirstOrDefaultAsync(m => m.Id == id);

            if (Class == null)
            {
                return NotFound();
            }

            ViewData["TeacherId"] = new SelectList(_context.Teachers.Include(t => t.User), "Id", "User.Name");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var @class = await _context.Classes.FindAsync(Class.Id);
            var teacher = await _context.Teachers.FindAsync(Class.TeacherId);

            await _context.Teachers.Where(t => t.ClassId == Class.Id).ForEachAsync(t => t.ClassId = null);
            await _context.Classes.Where(c => c.TeacherId == Class.TeacherId).ForEachAsync(c => c.TeacherId = null);

            teacher.ClassId = Class.Id;
            @class.TeacherId = Class.TeacherId;
            @class.Name = Class.Name;
            @class.Message = Class.Message;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClassExists(Class.Id))
                {
                    return NotFound();
                }

                throw;
            }

            return RedirectToPage("./Index");
        }

        private bool ClassExists(Guid id)
        {
            return _context.Classes.Any(e => e.Id == id);
        }
    }
}
