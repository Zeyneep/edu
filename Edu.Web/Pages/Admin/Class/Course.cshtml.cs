﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;
using Edu.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Pages.Admin.Class
{
    public class CourseModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public CourseModel(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @class = await _context.Classes.Include(c => c.Courses).FirstOrDefaultAsync(c => c.Id == id);
            if (@class == null)
            {
                return NotFound();
            }

            Class = new ClassViewModel
            {
                Id = @class.Id,
                CourseIds = @class.Courses.Select(c => c.CourseId)
            };

            ViewData["CourseId"] = new SelectList(_context.Courses, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public ClassViewModel Class { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var @class = await _context.Classes.FindAsync(Class.Id);
            if (@class == null)
            {
                return NotFound();
            }

            foreach (var classCourse in _context.ClassCourses)
            {
                _context.ClassCourses.Remove(classCourse);
            }

            if (Class.CourseIds != null)
            {
                foreach (var course in Class.CourseIds)
                {
                    _context.ClassCourses.Add(new ClassCourse
                    {
                        ClassId = Class.Id,
                        CourseId = course
                    });
                }
            }

            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}