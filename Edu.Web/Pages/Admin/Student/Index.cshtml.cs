﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Admin.Student
{
    public class IndexModel : PageModel
    {
        private readonly Data.ApplicationDbContext _context;

        public IndexModel(Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Data.Domain.Student> Student { get;set; }

        public async Task OnGetAsync()
        {
            Student = await _context.Students.Include(s => s.Class).Include(s => s.Parent.User).ToListAsync();
        }
    }
}
