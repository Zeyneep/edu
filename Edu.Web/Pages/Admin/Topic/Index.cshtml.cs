﻿using Edu.Web.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Edu.Web.Pages.Admin.Topic
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Data.Domain.Topic> Topic { get;set; }

        public async Task OnGetAsync()
        {
            Topic = await _context.Topics.Include(t => t.Course).ToListAsync();
        }
    }
}
