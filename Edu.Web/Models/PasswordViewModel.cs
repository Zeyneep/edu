﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Models
{
    public class PasswordViewModel
    {
        public Guid UserId { get; set; }
        [MinLength(3, ErrorMessage = "The {0} field should contain at least {1} characters.")]
        [Required(ErrorMessage = "The {0} field is required.")]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
