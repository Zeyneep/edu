﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Edu.Web.Data.Domain;

namespace Edu.Web.Models
{
    public class LessonViewModel
    {
        public Guid Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Date { get; set; }

        public IList<StudentViewModel> Students { get; set; }

        public class StudentViewModel
        {
            public Guid Id { get; set; }
            [Display(Name = "Name")]
            public string Name { get; set; }
            [Display(Name = "Late")]
            public bool Late { get; set; }
            [Display(Name = "Present")]
            public bool Present { get; set; }
            [Display(Name = "Behavior")]
            public StudentLesson.EBehavior? Behavior { get; set; }
            [Display(Name = "Note")]
            public string Note { get; set; }
        }
    }
}
