﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Models
{
    public class ClassViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Courses")]
        public IEnumerable<Guid> CourseIds { get; set; }
    }
}
