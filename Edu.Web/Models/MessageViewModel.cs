﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Models
{
    public class MessageViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Content")]
        public string Content { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        [Display(Name = "Receiver")]
        public string Receiver { get; set; }
        public bool Read { get; set; }
    }
}
