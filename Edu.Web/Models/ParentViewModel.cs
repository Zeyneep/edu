﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Edu.Web.Models
{
    public class ParentViewModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "The {0} field is required.")]
        [Remote("VerifyParentUserName", "Validation", ErrorMessage = "This user name already exists.", AdditionalFields = nameof(Id))]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The {0} field is required.")]
        public string Name { get; set; }
        [EmailAddress(ErrorMessage = "The {0} field is not a valid e-mail address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Phone(ErrorMessage = "The {0} field is not a valid phone number.")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}
