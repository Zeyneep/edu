﻿using System;

namespace Edu.Web.Models
{
    public class ReceiverViewModel
    {
        public Guid? UserId { get; set; }
        public string Name { get; set; }
    }
}
