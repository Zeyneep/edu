﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Edu.Web.Data.Domain;

namespace Edu.Web.Models
{
    public class StudentViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Student number")]
        public string StudentNumber { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Class")]
        public string Class { get; set; }
        [Display(Name = "Parent")]
        public string Parent { get; set; }
        public IList<CourseViewModel> Courses { get; set; }
        public IList<LessonViewModel> Lessons { get; set; }

        public class CourseViewModel
        {
            public Guid Id { get; set; }
            [Display(Name = "Name")]
            public string Name { get; set; }
            [Display(Name = "Materials")]
            public string Materials { get; set; }
            public IList<TopicViewModel> Topics { get; set; }

            public class TopicViewModel
            {
                public Guid Id { get; set; }
                [Display(Name = "Name")]
                public string Name { get; set; }
                [Display(Name = "Score")]
                public decimal? Score { get; set; }
                [Display(Name = "Completed")]
                public bool? Finished { get; set; }
                [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
                [Display(Name = "Date")]
                public DateTime? Date { get; set; }
            }
        }

        public class LessonViewModel
        {
            [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
            [Display(Name = "Date")]
            public DateTime Date { get; set; }
            [Display(Name = "Documents")]
            public IList<DocumentViewModel> Documents { get; set; }
            [Display(Name = "Late")]
            public bool Late { get; set; }
            [Display(Name = "Present")]
            public bool Present { get; set; }
            [Display(Name = "Behavior")]
            public StudentLesson.EBehavior Behavior { get; set; }
            [Display(Name = "Note")]
            public string Note { get; set; }

            public class DocumentViewModel
            {
                public Guid Id { get; set; }
                public string FileName { get; set; }
            }
        }
    }
}
