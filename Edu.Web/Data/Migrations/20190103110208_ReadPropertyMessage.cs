﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Edu.Web.Data.Migrations
{
    public partial class ReadPropertyMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EduUserMessage");

            migrationBuilder.AddColumn<bool>(
                name: "Read",
                table: "Message",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Read",
                table: "Message");

            migrationBuilder.CreateTable(
                name: "EduUserMessage",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    MessageId = table.Column<Guid>(nullable: false),
                    Read = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EduUserMessage", x => new { x.UserId, x.MessageId });
                    table.ForeignKey(
                        name: "FK_EduUserMessage_Message_MessageId",
                        column: x => x.MessageId,
                        principalTable: "Message",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EduUserMessage_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EduUserMessage_MessageId",
                table: "EduUserMessage",
                column: "MessageId");
        }
    }
}
