﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Edu.Web.Data.Migrations
{
    public partial class RemoveMessageFromLesson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Message",
                table: "Lessons");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Message",
                table: "Lessons",
                nullable: true);
        }
    }
}
