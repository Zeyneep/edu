﻿using System;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Data
{
    public class ApplicationDbContext : IdentityDbContext<EduUser, IdentityRole<Guid>, Guid>
    {
        public DbSet<Class> Classes { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<ClassCourse> ClassCourses { get; set; }
        public DbSet<StudentTopic> StudentTopics { get; set; }
        public DbSet<StudentLesson> StudentLessons { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EduUser>().Property(u => u.Name).IsRequired();

            modelBuilder.Entity<Class>().HasKey(c => c.Id);
            modelBuilder.Entity<Class>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Class>().HasIndex(c => c.Name).IsUnique();
            modelBuilder.Entity<Class>().Property(c => c.Name).IsRequired();
            modelBuilder.Entity<Class>().HasOne(c => c.Teacher).WithOne(t => t.Class).HasForeignKey<Teacher>(t => t.ClassId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Course>().HasKey(c => c.Id);
            modelBuilder.Entity<Course>().Property(c => c.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Course>().HasIndex(c => c.Name).IsUnique();
            modelBuilder.Entity<Course>().Property(c => c.Name).IsRequired();

            modelBuilder.Entity<ClassCourse>().HasKey(cc => new { cc.ClassId, cc.CourseId });
            modelBuilder.Entity<ClassCourse>().HasOne(cc => cc.Class).WithMany(c => c.Courses).HasForeignKey(cc => cc.ClassId);
            modelBuilder.Entity<ClassCourse>().HasOne(cc => cc.Course).WithMany(c => c.Classes).HasForeignKey(cc => cc.CourseId);

            modelBuilder.Entity<Topic>().HasKey(t => t.Id);
            modelBuilder.Entity<Topic>().Property(t => t.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Topic>().HasIndex(t => t.Name).IsUnique();
            modelBuilder.Entity<Topic>().Property(t => t.Name).IsRequired();
            modelBuilder.Entity<Topic>().HasOne(t => t.Course).WithMany(c => c.Topics).HasForeignKey(t => t.CourseId);

            modelBuilder.Entity<Lesson>().HasKey(l => l.Id);
            modelBuilder.Entity<Lesson>().Property(l => l.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Lesson>().HasOne(l => l.Class).WithMany(c => c.Lessons).HasForeignKey(l => l.ClassId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Teacher>().HasKey(t => t.Id);
            modelBuilder.Entity<Teacher>().Property(t => t.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Teacher>().HasOne(t => t.Class).WithOne(c => c.Teacher).HasForeignKey<Class>(c => c.TeacherId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Student>().HasKey(s => s.Id);
            modelBuilder.Entity<Student>().Property(s => s.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Student>().HasIndex(t => t.StudentNumber).IsUnique();
            modelBuilder.Entity<Student>().HasOne(s => s.Class).WithMany(c => c.Students).HasForeignKey(s => s.ClassId);
            modelBuilder.Entity<Student>().HasOne(s => s.Parent).WithMany(p => p.Students).HasForeignKey(s => s.ParentId);

            modelBuilder.Entity<StudentLesson>().HasKey(sl => new { sl.StudentId, sl.LessonId });
            modelBuilder.Entity<StudentLesson>().HasOne(sl => sl.Student).WithMany(s => s.Lessons).HasForeignKey(sl => sl.StudentId);
            modelBuilder.Entity<StudentLesson>().HasOne(sl => sl.Lesson).WithMany(l => l.Students).HasForeignKey(sl => sl.LessonId);

            modelBuilder.Entity<Parent>().HasKey(p => p.Id);
            modelBuilder.Entity<Parent>().Property(p => p.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<StudentTopic>().HasKey(st => new { st.StudentId, st.TopicId });
            modelBuilder.Entity<StudentTopic>().HasOne(st => st.Student).WithMany(s => s.Topics).HasForeignKey(st => st.StudentId);
            modelBuilder.Entity<StudentTopic>().HasOne(st => st.Topic).WithMany(t => t.Students).HasForeignKey(st => st.TopicId);

            modelBuilder.Entity<Message>().HasKey(m => m.Id);
            modelBuilder.Entity<Message>().Property(m => m.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Message>().Property(m => m.Title).IsRequired();
            modelBuilder.Entity<Message>().Property(m => m.Content).IsRequired();
            modelBuilder.Entity<Message>().HasOne(m => m.Sender).WithMany().HasForeignKey(m => m.SenderId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Message>().HasOne(m => m.Receiver).WithMany().HasForeignKey(m => m.ReceiverId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Document>().HasKey(d => d.Id);
            modelBuilder.Entity<Document>().Property(d => d.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Document>().HasOne(d => d.Lesson).WithMany(l => l.Documents).HasForeignKey(d => d.LessonId);
        }
    }
}
