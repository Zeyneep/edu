﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Edu.Web.Data.Domain;

namespace Edu.Web.Data
{
    public static class Seed
    {
        private static readonly string[] Roles = { Config.Roles.Administrator, Config.Roles.Teacher, Config.Roles.Parent };

        public static async Task Initialize(IServiceProvider provider)
        {
            IServiceScopeFactory scopeFactory = provider.GetRequiredService<IServiceScopeFactory>();
            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
                foreach (var role in Roles)
                {
                    if (!await roleManager.RoleExistsAsync(role))
                    {
                        await roleManager.CreateAsync(new IdentityRole<Guid>(role));
                    }
                }

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<EduUser>>();
                var admin = new EduUser("admin") { Name = "Admin" };
                if (await userManager.FindByNameAsync(admin.UserName) == null)
                {
                    await userManager.CreateAsync(admin, "N3n)zM_=6yC)");
                    await userManager.AddToRoleAsync(admin, Config.Roles.Administrator);
                }
            }
        }
    }
}
