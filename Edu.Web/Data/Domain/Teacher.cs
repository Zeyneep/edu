﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Data.Domain
{
    public class Teacher
    {
        public Guid Id { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Class")]
        public Guid? ClassId { get; set; }
        [Display(Name = "Class")]
        public Class Class { get; set; }
        [Display(Name = "Teacher")]
        public Guid UserId { get; set; }
        [Display(Name = "Teacher")]
        public EduUser User { get; set; }
    }
}
