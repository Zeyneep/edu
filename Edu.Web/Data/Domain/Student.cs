﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Edu.Web.Data.Domain
{
    public class Student
    {
        public Guid Id { get; set; }
        [Remote("VerifyStudentNumber", "Validation", ErrorMessage = "This student number already exists.", AdditionalFields = nameof(Id))]
        [Required(ErrorMessage = "The {0} field is required.")]
        [Display(Name = "Student number")]
        public string StudentNumber { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The {0} field is required.")]
        public string Name { get; set; }

        [Display(Name = "Class")]
        [Required(ErrorMessage = "The {0} field is required.")]
        public Guid ClassId { get; set; }
        [Display(Name = "Class")]
        public Class Class { get; set; }
        [Display(Name = "Parent")]
        [Required(ErrorMessage = "The {0} field is required.")]
        public Guid ParentId { get; set; }
        [Display(Name = "Parent")]
        public Parent Parent { get; set; }

        public ICollection<StudentLesson> Lessons { get; set; }
        public ICollection<StudentTopic> Topics { get; set; }
    }
}
