﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Edu.Web.Data.Domain
{
    public class Topic
    {
        public Guid Id { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The {0} field is required.")]
        [Remote("VerifyTopicName", "Validation", ErrorMessage = "This name already exists.", AdditionalFields = nameof(Id))]
        public string Name { get; set; }
        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Course")]
        public Guid CourseId { get; set; }
        [Display(Name = "Course")]
        public Course Course { get; set; }

        public ICollection<StudentTopic> Students { get; set; }
    }
}
