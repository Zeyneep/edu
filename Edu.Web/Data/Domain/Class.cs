﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Edu.Web.Data.Domain
{
    public class Class
    {
        public Guid Id { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The {0} field is required.")]
        [Remote("VerifyClassName", "Validation", ErrorMessage = "This name already exists.", AdditionalFields = nameof(Id))]
        public string Name { get; set; }
        [Display(Name = "Message")]
        public string Message { get; set; }

        [Display(Name = "Teacher")]
        public Guid? TeacherId { get; set; }
        [Display(Name = "Teacher")]
        public Teacher Teacher { get; set; }

        public ICollection<ClassCourse> Courses { get; set; }
        public ICollection<Student> Students { get; set; }
        public ICollection<Lesson> Lessons { get; set; }
    }
}
