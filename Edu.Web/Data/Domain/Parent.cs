﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Data.Domain
{
    public class Parent
    {
        public Guid Id { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Parent")]
        public Guid UserId { get; set; }
        [Display(Name = "Parent")]
        public EduUser User { get; set; }

        public ICollection<Student> Students { get; set; }
    }
}
