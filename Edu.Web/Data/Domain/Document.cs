﻿using System;

namespace Edu.Web.Data.Domain
{
    public class Document
    {
        public Guid Id { get; set; }
        public byte[] Data { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public Guid LessonId { get; set; }
        public Lesson Lesson { get; set; }
    }
}
