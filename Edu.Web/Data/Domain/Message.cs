﻿using System;

namespace Edu.Web.Data.Domain
{
    public class Message
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public bool Read { get; set; }

        public Guid SenderId { get; set; }
        public EduUser Sender { get; set; }
        public Guid ReceiverId { get; set; }
        public EduUser Receiver { get; set; }
    }
}
