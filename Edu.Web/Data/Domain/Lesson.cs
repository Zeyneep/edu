﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Data.Domain
{
    public class Lesson
    {
        public Guid Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [Display(Name = "Date")]
        [Required(ErrorMessage = "The {0} field is required.")]
        public DateTime Date { get; set; }

        [Display(Name = "Class")]
        public Guid ClassId { get; set; }
        [Display(Name = "Class")]
        public Class Class { get; set; }

        [Display(Name = "Documents")]
        public ICollection<Document> Documents { get; set; }
        public ICollection<StudentLesson> Students { get; set; }
    }
}
