﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Edu.Web.Data.Domain
{
    public class EduUser : IdentityUser<Guid>
    {
        public EduUser(string userName) : base(userName) { }

        [Display(Name = "Username")]
        public override string UserName { get; set; }

        [Display(Name = "Email")]
        public override string Email { get; set; }

        [Display(Name = "Phone number")]
        public override string PhoneNumber { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}
