﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Edu.Web.Data.Domain
{
    public class Course
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "The {0} field is required.")]
        [Remote("VerifyCourseName", "Validation", ErrorMessage = "This name already exists.", AdditionalFields = nameof(Id))]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Materials")]
        public string Materials { get; set; }

        public ICollection<ClassCourse> Classes { get; set; }
        public ICollection<Topic> Topics { get; set; }
    }
}
