﻿using System;

namespace Edu.Web.Data.Domain
{
    public class StudentTopic
    {
        public decimal? Score { get; set; }
        public bool? Finished { get; set; }
        public DateTime? Date { get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }
        public Guid TopicId { get; set; }
        public Topic Topic { get; set; }
    }
}
