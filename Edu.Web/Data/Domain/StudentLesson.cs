﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Edu.Web.Data.Domain
{
    public class StudentLesson
    {
        public bool? Late { get; set; }
        public bool? Present { get; set; }
        public EBehavior? Behavior { get; set; }
        public string Note { get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }
        public Guid LessonId { get; set; }
        public Lesson Lesson { get; set; }

        public enum EBehavior
        {
            [Display(Name = "Bad")]
            Bad,
            [Display(Name = "Normal")]
            Normal,
            [Display(Name = "Good")]
            Good
        }
    }
}
