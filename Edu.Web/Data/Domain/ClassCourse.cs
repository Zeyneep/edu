﻿using System;

namespace Edu.Web.Data.Domain
{
    public class ClassCourse
    {
        public Guid ClassId { get; set; }
        public Class Class { get; set; }
        public Guid CourseId { get; set; }
        public Course Course { get; set; }
    }
}
