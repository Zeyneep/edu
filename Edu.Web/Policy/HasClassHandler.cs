﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Edu.Web.Data;
using Edu.Web.Data.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Edu.Web.Policy
{
    public class HasClassHandler : AuthorizationHandler<HasClassRequirement>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EduUser> _userManager;

        public HasClassHandler(ApplicationDbContext context, UserManager<EduUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, HasClassRequirement requirement)
        {
            var user = await _userManager.GetUserAsync(context.User);
            var userId = await _userManager.GetUserIdAsync(user);

            var @class = await _context.Classes.SingleOrDefaultAsync(c => c.Teacher.UserId == new Guid(userId));
            if (@class != null)
            {
                context.Succeed(requirement);
            }
        }
    }
}
